<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output omit-xml-declaration="no" method="xml" version="1.0" standalone="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="b2bOpticJobData">
        <b2bOptic xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                  xsi:noNamespaceSchemaLocation="http://schema.b2boptic.com/b2boptic_lensorder_v1.6.3.xsd">
            <xsl:apply-templates select="*[not(self::element-to-ignore)]"/>
        </b2bOptic>
    </xsl:template>

    <xsl:template match="b2bOpticJobData/header">
        <xsl:copy>
            <xsl:copy-of select="attribute::*"/>
            <xsl:apply-templates select="customersOrderId"/>
            <xsl:if test="distributorsOrderId">
                <xsl:apply-templates select="distributorsOrderId"/>
            </xsl:if>
            <xsl:if test="not(distributorsOrderId)">
                <xsl:element name="distributorsOrderId"/>
            </xsl:if>
            <xsl:apply-templates select="timeStamps"/>
            <xsl:apply-templates select="remark"/>
            <xsl:apply-templates select="orderParties"/>
            <xsl:apply-templates select="software"/>
            <xsl:apply-templates select="productCatalog"/>
            <xsl:if test="portalOrderId">
                <xsl:apply-templates select="portalOrderId"/>
            </xsl:if>
            <xsl:if test="not(portalOrderId)">
                <xsl:element name="portalOrderId"/>
            </xsl:if>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="b2bOpticJobData/items/item/pair/lens">
        <xsl:copy>
            <xsl:copy-of select="attribute::*"/>
            <xsl:if test="commercialCode">
                <xsl:apply-templates select="commercialCode"/>
            </xsl:if>
            <xsl:if test="not(commercialCode)">
                <xsl:element name="commercialCode">
                </xsl:element>
            </xsl:if>
            <xsl:apply-templates select="remark"/>
            <xsl:apply-templates select="ignoreFlags"/>
            <xsl:if test="rxData">
                <xsl:apply-templates select="rxData"/>
            </xsl:if>
            <xsl:if test="not(rxData)">
                <xsl:element name="rxData">
                    <xsl:element name="sphere">NaN</xsl:element>
                </xsl:element>
            </xsl:if>
            <xsl:apply-templates select="coating"/>
            <xsl:apply-templates select="centration"/>
            <xsl:if test="geometry">
                <xsl:apply-templates select="geometry"/>
            </xsl:if>
            <xsl:if test="not(geometry)">
                <xsl:element name="geometry">
                    <xsl:element name="diameter">
                        <xsl:element name="physical">0</xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:if>
            <xsl:apply-templates select="options"/>
            <xsl:apply-templates select="frameFit"/>
            <xsl:apply-templates select="engraving"/>
            <xsl:apply-templates select="branding"/>
            <xsl:apply-templates select="lensPriceInfo"/>
            <xsl:apply-templates select="experimental"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="b2bOpticJobData/items/item/pair/lens/coating/coatingPriceInfo"/>

    <xsl:template match="b2bOpticJobData/items/item/pair/frame">
        <xsl:copy>
            <xsl:copy-of select="attribute::*"/>
            <xsl:if test="material">
                <xsl:apply-templates select="material"/>
            </xsl:if>
            <xsl:if test="not(material)">
                <xsl:element name="material">SPECIAL</xsl:element>
            </xsl:if>
            <xsl:apply-templates select="manufacturer"/>
            <xsl:apply-templates select="brand"/>
            <xsl:apply-templates select="model"/>
            <xsl:apply-templates select="size"/>
            <xsl:apply-templates select="color"/>
            <xsl:apply-templates select="branding"/>
            <xsl:apply-templates select="commercialCode"/>
            <xsl:apply-templates select="shape"/>
            <xsl:apply-templates select="boxWidth"/>
            <xsl:apply-templates select="boxHeight"/>
            <xsl:apply-templates select="distanceBetweenLenses"/>
            <xsl:apply-templates select="drillHoles"/>
            <xsl:apply-templates select="pantoscopicAngle"/>
            <xsl:apply-templates select="pantoscopicAngleRight"/>
            <xsl:apply-templates select="pantoscopicAngleLeft"/>
            <xsl:apply-templates select="frameBowAngle"/>
            <xsl:apply-templates select="frameCurve"/>
            <xsl:apply-templates select="frameReferenceId"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="b2bOpticJobData/items/item/pair/frame/framePriceInfo"/>
</xsl:stylesheet>
