b2bOptic JobData
------------------------------

A format to transfer incomplete lens job data from one system to another, very similar to b2bOptic LensOrder.

Documentation: http://wiki.b2boptic.com
Discussion: http://forum.b2boptic.com