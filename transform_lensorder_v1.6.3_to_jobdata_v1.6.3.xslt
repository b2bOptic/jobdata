<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output omit-xml-declaration="no" method="xml" version="1.0" standalone="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="b2bOptic">
        <b2bOpticJobData xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                         xsi:noNamespaceSchemaLocation="http://schema.b2boptic.com/b2boptic_jobdata_v1.6.3.xsd">
            <xsl:apply-templates select="*[not(self::element-to-ignore)]"/>
        </b2bOpticJobData>
    </xsl:template>

    <xsl:template match="b2bOptic/header/distributorsOrderId[not(descendant::text()[normalize-space()])]"/>

    <xsl:template match="b2bOptic/header/portalOrderId[not(descendant::text()[normalize-space()])]"/>

    <xsl:template match="b2bOptic/items/item/pair/lens/rxData/sphere[text() ='NaN']"/>
    <xsl:template match="rxData[sphere='NaN']"/>

    <xsl:template match="b2bOptic/items/item/pair/lens/rxDataFn/far/sphere[text() ='NaN']"/>
    <xsl:template match="rxDataFn/far[sphere='NaN']"/>

    <xsl:template match="b2bOptic/items/item/pair/lens/rxDataFn/near/sphere[text() ='NaN']"/>
    <xsl:template match="rxDataFn/near[sphere='NaN']"/>

    <xsl:template match="b2bOptic/items/item/pair/lens/geometry/diameter/physical[text() ='0']"/>
    <xsl:template match="geometry/diameter[physical='0']"/>

    <xsl:template match="b2bOptic/items/item/pair/lens/geometry/diameter/optical[text() ='0']"/>
    <xsl:template match="geometry/diameter[optical='0']"/>

    <xsl:template
            match="b2bOptic/items/item/pair/lens/geometry[not(descendant::text()[normalize-space()]) or descendant::text() = '0']"/>

    <xsl:template match="b2bOptic/items/item/pair/lens/commercialCode[not(descendant::text()[normalize-space()])]"/>

    <xsl:template match="b2bOptic/items/item/pair/patient/address[not(descendant::text()[normalize-space()])]"/>

    <xsl:template
            match="b2bOptic/items/item/stocklens/commercialCode[not(descendant::text()[normalize-space()])]"/>

    <xsl:template
            match="b2bOptic/items/item/stocklens/rxData[not(descendant::text()[normalize-space()]) or descendant::text()='NaN']"/>

    <xsl:template
            match="b2bOptic/items/item/stocklens/diameter[not(descendant::text()[normalize-space()]) or descendant::text() = '0']"/>

    <!--
    <xsl:template match="*[not(node())]"/>
    <xsl:template match="*[not(descendant::text()[normalize-space()])]"/>
    -->
</xsl:stylesheet>
